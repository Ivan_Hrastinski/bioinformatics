//
// Created by lcvetkovic on 26.11.2016..
//

#include <vector>

#ifdef HAVE_CONFIG_H
# include "sais_config.h"
#endif

#include <stdio.h>

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# ifdef HAVE_MEMORY_H
#  include <memory.h>
# endif
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#else
# ifdef HAVE_STRINGS_H
#  include <strings.h>
# endif
#endif
#if defined(HAVE_IO_H) && defined(HAVE_FCNTL_H)
# include <io.h>
# include <fcntl.h>
#endif

#include <time.h>

#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_INTTYPES_H
# include <inttypes.h>
#endif

#include "lfs.h"
#include "sais.h"

using namespace std;
#ifndef BIOINFORMATIKAC_NAIVE_H
#define BIOINFORMATIKAC_NAIVE_H

#endif //BIOINFORMATIKAC_NAIVE_H

#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXN 1000
#define MAXLOGN 10

class Preprocessor {
public:
    virtual int longestMatch(const vector<char> &T, int &pattern_index, int i) = 0;

    virtual int lca(int s1, int s2) = 0;

};

class NaivePreprocessor : public Preprocessor {
private:
    const vector<char> &P;

public:
    NaivePreprocessor(const vector<char> &P) :
            P(P) {

    }

    /**
     *
     * @param T Text
     * @param Pattern_index Start of pattern
     * @param i Offset
     * @return Integer lenght of the longest match
     */
    int longestMatch(const vector<char> &T, int &pattern_index, int i) {
        // cout<<"Search index : "<<i<<endl;
        int longest_substring = 0;
        bool found;
        int m = P.size();
        for (int l = 1; l <= m; ++l) {
            found = false;
            for (int j = 0; j <= m - l; ++j) {
                /*vector<char>::const_iterator first = T.begin() + i;
                vector<char>::const_iterator last = T.begin() + i + l;
                vector<char> T_sub(first, last);

                first = P.begin() + j;
                last = P.begin() + j + l;
                vector<char> P_sub(first, last);

                if (T_sub == P_sub) {
                    found = true;
                    longest_substring = l;
                    pattern_index = j;
                    break;
                }*/

                int k = 0;
                for (; k < l; ++k) {
                    if (P[i + k] != T[j + k]) {
                        break;
                    }
                }
                if (k == l + 1) {
                    found = true;
                    longest_substring = l;
                    pattern_index = j;
                    break;
                }
            }
            if (!found) {
                break;
            }
        }

        //cout<<"L : " <<longest_substring <<" Index : "<<pattern_index<<endl;
        return longest_substring;
    }


    /**
     *
     * @param s1 Start index of first array
     * @param s2 Start index of second array
     * @return LCA length
     */
    int lca(int s1, int s2) {
        int count = 0;
        int d = 0;
        int m = P.size();
        while ((s1 + d) < m && (s2 + d) < m && P[s1 + d] == P[s2 + d])
            ++d;
        //cout<<"d : "<<d << endl;
        //cout<<"m : "<<m << endl;


        return d;
    }

};

class SAPreprocessor : public Preprocessor {
private:
    //Pattern
    const vector<char> &P;
    //Text
    const vector<char> &T;
    //LCP array created with kassais algorithm
    vector<int> lcpKas;
    //sa_int32_t *SAT;
    //Pattern suffix array
    sa_int32_t *SAP;

    int* pos;
    //Inverze suffix array for pattern
    vector<int> iSAP;
    //Text lenght
    int n;
    //Pattern lenght
    int m;
    // Min range matrix for RMQ
    int **minRange;

public:
    // init and build all the needed arrays
    SAPreprocessor(const vector<char> &T, const vector<char> &P) :
            P(P), T(T) {
        n = T.size();
        m = P.size();
        //SAT = (sa_int32_t *) malloc((size_t) n * sizeof(sa_int32_t));
        SAP = (sa_int32_t *) malloc((size_t) m * sizeof(sa_int32_t));
        //sais_u8((const sa_uint8_t *) T.data(), SAT, (sa_int32_t) n, 256);
        sais_u8((const sa_uint8_t *) P.data(), SAP, (sa_int32_t) m, 256);
        /*cout << "Suffix array Text:\t";
        for (int i = 0; i < n; i++) {
            cout << SAT[i] << " ";

        }
        cout << "\n Suffix array Patern:\t";
        for (int i = 0; i < m; i++) {
            cout << SAP[i] << " ";

        }
        cout << "\n LCP Table:\t";*/
        lcpKas = kasai(P, SAP);
        //Compute_ST(lcpKas, m);
        cout << "\n";
        preprocessMinRange();
        lcpKas.clear();
    }

    char charAt(int pos) {
        return pos < m ? P[pos] : (char) 0;
    }


    void narrowInterval(int lo, int hi, char key, int depth, int &nlo, int &nhi) {
        while (lo < hi) {
            int k = (lo + hi) >> 1;
            char c = charAt(SAP[k] + depth);
            if (c < key) {
                lo = k + 1;
            } else if (c > key) {
                hi = k;
            } else {
                if (charAt(SAP[lo] + depth) == charAt(SAP[hi - 1] + depth))
                    break;
                int tmp;
                narrowInterval(lo, k, key, depth, nlo, tmp);
                narrowInterval(k + 1, hi, key, depth, tmp, nhi);
                return;
            }
        }
        nlo = lo;
        nhi = hi;
    }

    /**
       *
       * @param T Text
       * @param Pattern_index Start of pattern
       * @param i Offset
       * @return Integer lenght of the longest match
       */
    int longestMatch(const vector<char> &T, int &suffix, int i) {
        int iter = i;
        int lo = 0;
        int hi = m;
        do {
            int nlo, nhi;
            narrowInterval(lo, hi, T[iter], iter-i, nlo, nhi);
            if (nlo < nhi) {
                ++iter;
                lo = nlo;
                hi = nhi;
            } else
                break;
        } while (iter < T.size() - i);
        suffix = SAP[lo];
        return iter - i;
    }

    int longestMatch3(const vector<char> &T, int &pattern_index, int i) {
        // cout<<"Search index : "<<i<<endl;
        int longest_substring = 0;
        bool found;
        int m = P.size();
        for (int l = 1; l <= m; ++l) {
            found = false;
            for (int j = 0; j <= m - l; ++j) {
                int k = 0;
                for (; k < l; ++k) {
                    if (P[i + k] != T[j + k]) {
                        break;
                    }
                }
                if (k == l + 1) {
                    found = true;
                    longest_substring = l;
                    pattern_index = j;
                    break;
                }
            }
            if (!found) {
                break;
            }
        }

        //cout<<"L : " <<longest_substring <<" Index : "<<pattern_index<<endl;
        return longest_substring;
    }
    /**
       *
       * @param T Text
       * @param suffix Start of pattern
       * @param i Offset
       * @return Integer lenght of the longest match
       */
    int longestMatch4(const vector<char> &T,int &suffix, int n) {
/*
		 // naive
		 int max = 0;
		 for (int i = 0; i < m; ++i) {
		 int *c = t;
		 int j = 0;
		 while (i + j < m && j < n && *c == p[i + j]) {
		 ++j;
		 ++c;
		 }
		 if (j > max) {
		 max = j;
		 suffix = i;
		 }
		 }
*/
        int i = n;
        int lo = 0;
        int hi = m;
        do {
            int nlo, nhi;
            narrowInterval(lo, hi, T[i], i, nlo, nhi);
            if (nlo < nhi) {
                ++i;
                lo = nlo;
                hi = nhi;
            } else
                break;
        } while (i < T.size());
        suffix = SAP[lo];
//				if (i != max)
//					printf("Problem - longest match should be %d but is %d\n", max, i);
        return i-n;
        //		return max;
    }
    // log with base 2
    int intLog(int n) {
        int b = 0;
        if (n >= 0x10000) {
            b += 16;
            n >>= 16;
        }
        if (n >= 0x100) {
            b += 8;
            n >>= 8;
        }
        if (n >= 0x10) {
            b += 4;
            n >>= 4;
        }
        if (n >= 0x4) {
            b += 2;
            n >>= 2;
        }
        if (n >= 0x2) {
            b += 1;
            n >>= 1;
        }
        return b;
    }
    // Creates the RMQ matrix for later queries
    void preprocessMinRange() {
        minRange = new int*[m];
        int size = 2 + intLog(m);
        for (int i = m - 1; i >= 0; --i) {
            minRange[i] = new int[size];
            minRange[i][0] = lcpKas[i];

            for (int b = 0, eb = 1; i + eb < m; ++b, eb <<= 1)
                minRange[i][b + 1] = min(minRange[i][b], minRange[i + eb][b]);
        }
    }
    //Kassais algorithm for LCP array creation
    vector<int> kasai(const vector<char> &s, sa_int32_t *sa) {

        // To store LCP array
        vector<int> lcp(m, 0);

        // An auxiliary array to store inverse of suffix array
        // elements. For example if suffixArr[0] is 5, the
        // invSuff[5] would store 0.  This is used to get next
        // suffix string from suffix array.
        vector<int> invSuff(m, 0);

        // Fill values in invSuff[]
        for (int i = 0; i < m; i++)
            invSuff[sa[i]] = i;

        // Initialize length of previous LCP
        int k = 0;

        // Process all suffixes one by one starting from
        // first suffix in txt[]
        for (int i = 0; i < m; i++) {
            /* If the current suffix is at n-1, then we don’t
               have next substring to consider. So lcp is not
               defined for this substring, we put zero. */
            if (invSuff[i] == m - 1) {
                k = 0;
                continue;
            }

            /* j contains index of the next substring to
               be considered  to compare with the present
               substring, i.e., next string in suffix array */
            int j = sa[invSuff[i] + 1];

            // Directly start matching from k'th index as
            // at-least k-1 characters will match
            while (i + k < m && j + k < m && s[i + k] == s[j + k])
                k++;

            lcp[invSuff[i]] = k; // lcp for the present suffix.

            // Deleting the starting character from the string.
            if (k > 0)
                k--;
        }

        // return the constructed lcp array
        iSAP = invSuff;
        return lcp;
    }

    int M[MAXN][MAXLOGN];

    void Compute_ST(const vector<int> &A, int N) {
        int i, j;
        for (i = 0; i < N; i++)
            M[i][0] = i;
        for (j = 1; 1 << j <= N; j++) {
            for (i = 0; i + (1 << (j - 1)) < N; i++) {
                if (A[M[i][j - 1]] <= A[M[i + (1 << (j - 1))][j - 1]])
                    M[i][j] = M[i][j - 1];
                else
                    M[i][j] = M[i + (1 << (j - 1))][j - 1];
            }
        }
    }

    int RMQ(vector<int> A, int s, int e) {
        int k = e - s;
        k = 31 - __builtin_clz(k + 1); // k = log(e-s+1)
        if (A[M[s][k]] <= A[M[e - (1 << k) + 1][k]])
            return A[M[s][k]];
        return A[M[e - (1 << k) + 1][k]];
    }

    void printDebug(vector<char> P, vector<char> T) {
        cout << "P: ";
        cout << "\n" << "T: ";
        cout << '\n';
    }

    static int pstrcmp(const void *a, const void *b) {
        //printf("pstrcmp: %s %sn", (const char*)*(char **)a, (const char*)*(char **)b);
        return strcmp((const char *) *(char **) a, (const char *) *(char **) b);
    }

    int lcp(char *a, char *b) {
        int len1, len2;
        int i;
        len1 = strlen(a);
        len2 = strlen(b);
        for (i = 0; (i < len1) && (i < len2); ++i) {
            if (a[i] != b[i]) break;
        }
        return i;
    }
    /**
       *
       * @param T Text
       * @param Pattern_index Start of pattern
       * @param i Offset
       * @return Integer lenght of the longest match
       */
    int longestMatch2(vector<char> T, int &pattern_index, int i) {

        //cout<<"Search index : "<<i<<endl;
        char firstText[P.size()];
        char firstPattern[P.size()];
        int lastChar = i + P.size();
        for (int k = i; k < lastChar; ++k) {
            firstText[k-i] = T[k];
        }
        firstText[P.size()]='\0';

        for (int k = 0; k < P.size(); ++k) {
            firstPattern[k] = P[k];
        }

        firstPattern[P.size()]='\0';


        int j;
        char **ap;      //suffix pointers array
        int len1, len2;
        char *cstr;
        int lcslen = 0, lcplen, lcssufpos = -1;
        len1 = strlen(firstPattern);
        len2 = strlen(firstText);
        ap = (char **) malloc((len1 + len2) * sizeof(char *));
        cstr = strcat(firstPattern, firstText);
        for (j = 0; j < len1 + len2; ++j) {
            ap[j] = &(cstr[j]);
        }

        qsort(ap, len1 + len2, sizeof(char *), pstrcmp);

        for (j = 0; j < len1 + len2 - 1; ++j) {
            if ((ap[j] - cstr >= len1) && (ap[j + 1] - cstr >= len1)) {
                //both starts with suffix of second string
                continue;
            } else if ((ap[j] - cstr < len1) && (ap[j + 1] - cstr < len1)) {
                //both starts with suffix of first string
                continue;
            } else {
                lcplen = lcp(ap[j], ap[j + 1]);
                if (lcplen > lcslen) {
                    lcslen = lcplen;
                    lcssufpos = j;
                }
            }
        }

        char* str = (ap[lcssufpos]);
        str [lcplen] = '\0';
        string pattern(P.begin(), P.end());
        char * pch;
        pch=strchr(str,*str);

        int pat_ind = pch-str;

/*        pattern_index = len1+len2-strlen(ap[lcssufpos]);
        cout<<ap[lcssufpos]<<endl;
        cout<<"L : " <<lcslen <<" Index : "<<pattern_index<<endl;*/

        pattern_index= pat_ind;

        return lcslen;
    }


    int rmq(int l, int r) {
        if (r < l) {
            int tmp = l;
            l = r;
            r = tmp;
        }
        int bit = intLog(r - l);
        return min(minRange[l][bit], minRange[r - (1 << bit)][bit]);
    }
    /**
     *
     * @param A Array for RMQ
     * @param s start index
     * @param e end index
     * @return RMQ
     */
    int NaiveRMQ(vector<int> A, int s, int e) {
        int minindex = s;
        for (int i = s; i <= e; i++)
            if (A[i] < A[minindex])
                minindex = i;
        return A[minindex];
    }

    /**
     *
     * @param s1 Start index of first array
     * @param s2 Start index of second array
     * @return LCA length
     */
    int lca(int s1, int s2) {
        int r = m - s1;

        if (s1 == m || s2 == m)
            return 0;
        int rmqres = m - s1;
        if (s1 != s2) {
            if (s1 > s2) {
                //rmqres = RMQ(lcpKas,iSAP[s2],iSAP[s1]);
                //rmqres = NaiveRMQ(lcpKas,iSAP[s2],iSAP[s1]);
                rmqres = rmq(iSAP[s2], iSAP[s1]);
            } else {
                //rmqres = RMQ(lcpKas,iSAP[s1],iSAP[s2]);
                //rmqres = NaiveRMQ(lcpKas,iSAP[s1],iSAP[s2]);
                rmqres = rmq(iSAP[s1], iSAP[s2]);
            }
            int lcpres = lcpKas[rmqres];
            return rmqres;
        }

        return rmqres;
    }

};

