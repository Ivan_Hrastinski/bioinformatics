# KMismatchAlgorithm


The tool should be compatible with most UNIX flavors and has been successfully tested on the following operating systems:

- Mac OS X 10.11.1
- Mac OS X 10.10.3
- Ubuntu 14.04 LTS

## Requirements

To run the kmismatch  correctly, the executables for the programs listed below should be reachable from the `PATH` variable of your shell.

- [g++][2] (4.9.0 or later)
- [CMake][3] (2.8 or later)



## Dependencies

The kmismatch depends directly on 1 library:

- [SAIS][1]




## Installation

Before the installation of the KMismatchAlgorithm you have to donwload and make the sais[1] library. After you download it you have to run cmake to create the executables.
And copy the made files into the project directory

To install KMismatchAlgorithm run the following commands from the folder where you want to install the tool:

	git clone https://Ivan_Hrastinski@bitbucket.org/Ivan_Hrastinski/bioinformatics.git
	cd bioinformatics/
	git submodule update --init --recursive
    cp -R /path/to/sais .
	cd sais
    cmake ./
    make
    cd ..
    cmake ./
    make





## Usage

To run the tool please use the command format shown below:

	./_BioInformatikaC  <kmis.in>



###Arguments:

 1. **kmis.in: file in which are the input arguments

        File format:

        m n k

        P

        T

            in which

            m - pattern lenght

            n - text lenght

            k - number of maximum allowed mismatches

            P - Pattern

            T - Text

### Examples:

**1)**	`./_BioInformatikaC  kmis.in`

The above command will run the kmismatch over the input text in `kmis.in` using the pattern providet in the file.


## Contributors

- [Luka Cvetkovic](luka.cvetkovic@fer.hr)
- [Filip Culinovic](filip.culinovic@fer.hr)
- [Ivan Hrastinski](ivan.hrastinski@fer.hr)

[1]: https://sites.google.com/site/yuta256/sais "sais"
[2]: https://cmake.org/ "CMake"
[3]: https://gcc.gnu.org "g++"


