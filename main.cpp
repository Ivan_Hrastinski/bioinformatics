#include <iostream>
#include <vector>
#include <set>
#include <cstdio>
#include <cstdlib>
#include <list>
#include "preprocessor.h"
#include <algorithm>
#include <iterator>

void
updateMism(int k, const vector<char> &T, const vector<char> &P, Preprocessor &preprocessor, int n, int m, int i, int sm,
           int l, int alignStart, int j, vector<int> &M, list<int>::iterator &s);

using namespace std;


template<typename T>
void printVector(string msg, const T &t) {
    cout << msg;
    std::copy(t.cbegin(), t.cend(), std::ostream_iterator<typename T::value_type>(std::cout, " "));
    cout << endl;
}

template<typename T>
void printVectorInVector(const T &t) {
    std::for_each(t.cbegin(), t.cend(), printVector<typename T::value_type>);
}

void readInput(char *inFile, vector<char> &T, int &n, vector<char> &P, int &m, int &k) {
    FILE *f = fopen(inFile, "r");
    if (f == NULL) {
        cout << "Cannot open file " << inFile << endl;
        exit(0);
    }
    fscanf(f, "%d %d %d\n", &m, &n, &k);
    P = vector<char>(m);
    char d;
    for (int i = 0; i < m; ++i) {
        fscanf(f, "%c ", &d);
        P[i] = d;
    }

    T = vector<char>(n);
    for (int i = 0; i < n; ++i) {
        fscanf(f, "%c ", &d);
        T[i] = d;
    }
    fclose(f);
}

void output(FILE *f, vector<int> a, int n) {
    for (int i = 0; i < n; ++i)
        fprintf(f, "%d ", a[i]);
    fprintf(f, "\n");
}


vector<int> naiveCalc(vector<char> T, vector<char> P) {
    int m = P.size();
    int n = T.size();

    std::vector<int> M(n, 0);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m & (j + i) < n; j++) {
            if (T[i + j] != P[j]) {
                M[i]++;
            }
        }
    }
    return M;

}

void updateMism(int k, const vector<char> &T, const vector<char> &P, Preprocessor &preprocessor, int n, int m, int i, int sm,
           int l, int alignStart, int j, vector<int> &M, list<int>::iterator &s) {
    while (j <= l && alignStart + j < m) {
        int s1 = alignStart + j;
        int s2 = sm + j;
        j += preprocessor.lca(s1, s2);
        if (j < l) {
            if (alignStart + j < m)
                ++M[*s];
        } else if (alignStart + l < m && i + l < n && T[i + l] != P[alignStart + l])
            M[*s]++;

        if (M[*s] > k)
            break;
        ++j;
    } ;
}


vector<int> nkCountMismatches(int k, const vector<char> &T, const vector<char> &P, Preprocessor &preprocessor) {

    int n = T.size();
    int m = P.size();

    std::vector<int> M(n, 0);

    list<int> q;
    for (int i = 0; i < n; ++i) {
        int sm = 0;
        int l = preprocessor.longestMatch(T, sm, i);

        for (int j = 0; j <= l; ++j) {
            if (i + j < n) {
                q.push_back(i + j);
            }

        }

        for (list<int>::iterator it = q.begin(); it != q.end();) {
            int alignStart = i - *it;
            if (alignStart < m) {
                int j = alignStart < 0 ? -alignStart : 0;
                do {
                    j += preprocessor.lca(alignStart + j, sm + j);
                    if (j < l) {
                        if (alignStart + j < m)
                            ++M[*it];
                    } else if (alignStart + l < m && i + l < n && T[i + l]
                                                                  != P[alignStart + l])
                        M[*it]++;

                    if (M[*it] > k)
                        break;
                    ++j;
                } while (j <= l && alignStart + j < m);
            }
            if (M[*it] > k || alignStart >= m) {
                it = q.erase(it);
            } else
                it++;
        }
        i += l;
    }
    return M;
}

int main(int argc, char **argv) {

    vector<char> T, P;
    int m, n, k;
    readInput(argv[1], T, n, P, m, k);

    // printVector("Text    ", T);
    // printVector("Pattern ", P);
    // cout << "k = " << k << endl;

    NaivePreprocessor pp(P);
    SAPreprocessor SAP(T, P);
    // cout<<"NAIVE -------------------------"<<endl;
     vector<int> Mnp = nkCountMismatches(k, T, P, pp);
    // cout<<"SAP -------------------------"<<endl;
    vector<int> Mnp2 = nkCountMismatches(k, T, P, SAP);

    //vector<int> Mn = naiveCalc(T, P);

     printVector("NaivePreprocessor \t", Mnp);
     printVector("SAPreprocessor2 \t", Mnp2);
    // printVector("Naive \t\t\t", Mn);
    return 0;
}

